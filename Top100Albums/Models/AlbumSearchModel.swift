//
//  AlbumSearchModel.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import Foundation

class AlbumSearchModel {
    
    let iTunesAlbumsURL = URL(string: "https://rss.itunes.apple.com/api/v1/us/apple-music/top-albums/all/100/explicit.json")
    
    func retrieveTopAlbums(completionHandler: @escaping (ResponseBody?, Error?) -> ()) {
        guard let url = iTunesAlbumsURL else { return }
        DispatchQueue.main.async {
            NetworkManager.get(url, objectType: ResponseBody.self) { (result: NetworkManager.Result<ResponseBody>) in
                switch result {
                case .success(let albums):
                    completionHandler(albums, nil)
                case .failure(let error):
                    switch error {
                    case .deserializationError, .unknown:
                        print(error)
                    case .unexpectedResponse:
                        print("Unknown error.")
                    }
                    completionHandler(nil, nil)
                }
            }
        }
    }
    
}
