//
//  AlbumDetailViewModel.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import Foundation

enum Titles: String {
    case artist = "Artist:"
    case album = "Album:"
    case genre = "Genre:"
    case releaseDate = "Release Date:"
    case copyright = "Copyright:"
}

struct StackModel {
    let title: String
    let detail: String?
}

struct AlbumDetailsModel {
    var rows: [StackModel] = []
    
    init(data: Album) {
        rows = generateStackModel(album: data)
    }
    
    private func generateStackModel(album: Album) -> [StackModel] {
        var stackModel: [StackModel] = []
        stackModel.append(StackModel(title: Titles.artist.rawValue, detail: album.artistName))
        stackModel.append(StackModel(title: Titles.album.rawValue, detail: album.name))
        if let genres = album.genres {
            let genreArrayString = (genres.map({$0.name ?? ""})).joined(separator: ", ").capitalized
            stackModel.append(StackModel(title: Titles.genre.rawValue, detail: genreArrayString))
        }
        stackModel.append(StackModel(title: Titles.releaseDate.rawValue, detail: album.releaseDate))
        stackModel.append(StackModel(title: Titles.copyright.rawValue, detail: album.copyright))
        
        return stackModel
    }
}

