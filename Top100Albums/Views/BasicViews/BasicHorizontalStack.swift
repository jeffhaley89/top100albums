//
//  BasicHorizontalStack.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class BasicHorizontalStack: UIStackView {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        assignGenericProperties()
    }
    
    private func assignGenericProperties() {
        self.axis = .horizontal
        self.distribution = .fill
        self.alignment = .top
        self.spacing = 4
    }
    
}
