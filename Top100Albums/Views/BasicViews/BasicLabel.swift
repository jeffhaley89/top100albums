//
//  BasicLabel.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class BasicLabel: UILabel {
   
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        assignGenericProperties()
    }
    
    func assignGenericProperties() {
        self.textColor = .black
        self.font = UIFont.systemFont(ofSize: 14)
        self.textAlignment = .left
        self.numberOfLines = 0
    }
}

