//
//  AlbumTableViewCell.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    let albumImageView = AlbumImageView(frame: .zero)
    
    private let artistLabel = BasicLabel()
    private let albumLabel = BasicLabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        designAlbumCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func designAlbumCell() {
        addSubview(artistLabel)
        addSubview(albumLabel)
        addSubview(albumImageView)
        artistLabel.font = UIFont.systemFont(ofSize: 16)
        albumImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        albumImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        let verticalStack = UIStackView(arrangedSubviews: [artistLabel, albumLabel])
        verticalStack.axis = .vertical
        verticalStack.distribution = .fillProportionally
        verticalStack.spacing = 4
        
        let horizontalStack = UIStackView(arrangedSubviews: [albumImageView, verticalStack])
        horizontalStack.axis = .horizontal
        horizontalStack.alignment = .top
        horizontalStack.distribution = .fillProportionally
        horizontalStack.spacing = 8
        addSubview(horizontalStack)
        
        horizontalStack.translatesAutoresizingMaskIntoConstraints = false
        horizontalStack.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        horizontalStack.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        horizontalStack.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        horizontalStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
    }
    
    func configureAlbumCell(album: Album?) {
        guard let album = album else { return }
        albumImageView.image = nil
        artistLabel.text = album.artistName
        albumLabel.text = album.name
        albumImageView.setImage(imageURL: album.artworkUrl100)
    }
    
}
