//
//  AlbumDetailView.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

protocol AlbumDetailButtonDelegate: class {
    func albumDetailButtonPressed()
}

class AlbumDetailView: UIView {
    
    var buttonDelegate: AlbumDetailButtonDelegate?
    
    let albumImageView = AlbumImageView(frame: .zero)
    
    private let iTunesButton: UIButton = {
        let button = UIButton()
        button.setTitle("View in iTunes", for: .normal)
        button.backgroundColor = UIColor.orange
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        return button
    }()
    
    private var stackModel: [StackModel] = []
    
    init(frame: CGRect, album: Album?, detailButtonDelegate: AlbumDetailButtonDelegate) {
        super.init(frame: frame)

        configureView(with: album)
        buttonDelegate = detailButtonDelegate
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView(with album: Album?) {
        backgroundColor = UIColor.white
        if let album = album {
            stackModel = AlbumDetailsModel(data: album).rows
            albumImageView.setImage(imageURL: album.artworkUrl100)
        }
        
        let verticalStackView: UIStackView = {
            let verticalStack = UIStackView()
            verticalStack.axis = .vertical
            verticalStack.distribution = .fillProportionally
            verticalStack.spacing = 8
            
            for row in stackModel.enumerated() {
                let titleLabel = BasicLabel()
                titleLabel.text = row.element.title
                titleLabel.setContentHuggingPriority(.required, for: .horizontal)
                let detailLabel = BasicLabel()
                detailLabel.text = row.element.detail
                verticalStack.addArrangedSubview(BasicHorizontalStack(arrangedSubviews: [titleLabel, detailLabel]))
            }
            return verticalStack
        }()
        addSubview(albumImageView)
        addSubview(verticalStackView)
        addSubview(iTunesButton)
        
        albumImageView.translatesAutoresizingMaskIntoConstraints = false
        albumImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        albumImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 120).isActive = true
        albumImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -120).isActive = true
        albumImageView.heightAnchor.constraint(equalTo: albumImageView.widthAnchor, multiplier: 1.0).isActive = true
        albumImageView.bottomAnchor.constraint(equalTo: verticalStackView.topAnchor, constant: -16).isActive = true
        
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        verticalStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        iTunesButton.translatesAutoresizingMaskIntoConstraints = false
        iTunesButton.center.x = self.center.x
        iTunesButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        iTunesButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        iTunesButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        iTunesButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func buttonPressed() {
        buttonDelegate?.albumDetailButtonPressed()
    }
}
