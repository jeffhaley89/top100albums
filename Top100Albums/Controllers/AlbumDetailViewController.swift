//
//  AlbumDetailViewController.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class AlbumDetailViewController: UIViewController {
    
    var album: Album?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Album Details"
        self.view.addSubview(AlbumDetailView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), album: album, detailButtonDelegate: self))
    }
}

extension AlbumDetailViewController: AlbumDetailButtonDelegate {
    func albumDetailButtonPressed() {
        guard let album = album, let name = album.name, let id = album.id else { return }
        if let urlString = "itms://itunes.apple.com/us/album/\(name)/id\(id)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

