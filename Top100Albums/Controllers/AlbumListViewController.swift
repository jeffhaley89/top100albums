//
//  AlbumListViewController.swift
//  Top100Albums
//
//  Created by Jeffrey Haley on 8/6/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

let albumImageCache = NSCache<AnyObject, AnyObject>()

class AlbumListViewController: UIViewController {
    
    var albums: [Album]? {
        didSet {
            albumListTableView.reloadData()
        }
    }
    
    enum AlbumCellId: String {
        case album = "AlbumTableViewCell"
    }
    
    private var albumListTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.register(AlbumTableViewCell.self, forCellReuseIdentifier: AlbumCellId.album.rawValue)
        return tableView
    }()
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        designView()
        getAlbums()
    }
    
    private func designView() {
        title = "Top 100 Albums"
        
        view.addSubview(activityIndicator)
        activityIndicator.center = self.view.center
        
        view.addSubview(albumListTableView)
        albumListTableView.dataSource = self
        albumListTableView.delegate = self
        albumListTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        albumListTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        albumListTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        albumListTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func getAlbums() {
        activityIndicator.startAnimating()
        AlbumSearchModel().retrieveTopAlbums { [weak self] (result: ResponseBody?, error: Error?) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.albums = result?.feed.albums
            }
        }
    }
}

extension AlbumListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let albumCell = tableView.dequeueReusableCell(withIdentifier: AlbumCellId.album.rawValue) as? AlbumTableViewCell else { return UITableViewCell() }
        albumCell.configureAlbumCell(album: self.albums?[indexPath.row])
        return albumCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let selectedAlbum = albums?[indexPath.row] else { return }
        let albumDetailVC = AlbumDetailViewController()
        albumDetailVC.album = selectedAlbum
        self.navigationController?.pushViewController(albumDetailVC, animated: true)
    }
}
